from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.shortcuts import render, redirect


def index(request):
    return render(request, 'registration/index.html')


def custom_login(request):
    context = {}

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('index')
        else:
            context.update({'msg': 'Usuario o contraseña incorrecta'})

    return render(request, 'registration/custom_login.html', context)


def custom_register(request):
    context = {}
    if request.method == 'POST':
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)

        if email and password:
            try:
                usuario = User.objects.create_user(username=email, email=email, password=password)
                context.update({'msg': f'Usuario {usuario.username}  creado correctamente'})
            except Exception as e:
                context.update({'msg': 'No se pudo completar el registro intentelo de nuevo'})
                print(e)

    return render(request, 'registration/custom_register.html', context)


def custom_logout(request):
    logout(request)
    return redirect('index')


def custom_change_password(request):
    context = {}
    if request.method == 'POST':
        password = request.POST.get('password', None)
        password2 = request.POST.get('password2', None)
        if password == password2:
            # cambio de contraseña
            usuario = request.user
            usuario.set_password(password)
            usuario.save()
            context.update({'msg': 'Contraseña cambiada con exito'})
        else:
            context.update({'msg': 'Las contraseñas que estas ingresando no son iguales'})

    return redirect('index')


def django_register(request):
    form = UserCreationForm()

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')

    context = {'form': form}
    return render(request, 'registration/django_register.html', context)


def django_login(request):
    form = AuthenticationForm()

    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            login(request, form.user_cache)
            return redirect('index')

    return render(request, 'registration/django_login.html', {'form': form})
