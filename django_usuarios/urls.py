from django.contrib import admin
from django.urls import path, include

from registration.views import *

urlpatterns = [
    path('', index, name='index'),
    path('admin/', admin.site.urls),
    path('custom-login', custom_login, name='custom_login'),
    path('custom-register', custom_register, name='custom_register'),
    path('custom-logout', custom_logout, name='custom_logout'),
    path('custom-change-password', custom_change_password, name='custom_change_password'),

    # usuarios con formularios django
    path('django-register', django_register, name='django_register'),
    path('django-login', django_login, name='django_login'),

    # vistas incluidas de django
    path('accounts/', include('django.contrib.auth.urls')),
]
